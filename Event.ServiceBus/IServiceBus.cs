﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.ServiceBus
{
    public interface  IServiceBus
    {
        void Publish<TMessage>(TMessage message);
    }
}
