﻿using Castle.Windsor;

namespace Event.ServiceBus
{
    public class ServiceBus:IServiceBus
    {
        private readonly IWindsorContainer _container;

        public ServiceBus(IWindsorContainer container)
        {
            _container = container;
        }

        public void Publish<TMessage>(TMessage message)
        {
            var handlers = _container.ResolveAll<IMessageHandler<TMessage>>();
            foreach (var messageHandler in handlers)
            {
                messageHandler.Handle(message);

                _container.Release(messageHandler);
            }
        }
    }
}