﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Event.MemoryLeak.Properties;

namespace Event.MemoryLeak
{
    public partial class MemoryLeakForm : Form
    {
        public readonly List<EventPublisher> LongLivedEventPublishers = new List<EventPublisher>();
        public readonly List<StaticEventPublisher> StaticEventPublishers = new List<StaticEventPublisher>(); 
        public MemoryLeakForm()
        {
            InitializeComponent();
        }


        private void btnStartShortTimePublisherTest_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < 100; i++)
            {
                var publisher = new EventPublisher();
                publisher.OnSomething += new Subscriber().ShowMessage;
                publisher.TriggerSomething();
            }

            MessageBox.Show(string.Format("There are {0} publishers in memory, {1} subscribers in memory", EventPublisher.Count, Subscriber.Count));
            GC.Collect();
        }

        private void BtnStartLongTimePublisher_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < 100; i++)
            {
                var publisher = new EventPublisher();
                publisher.OnSomething += new Subscriber().ShowMessage;
                publisher.TriggerSomething();
                LongLivedEventPublishers.Add(publisher);
            }
            MessageBox.Show(string.Format("There are {0} publishers in memory, {1} subscribers in memory", EventPublisher.Count,Subscriber.Count));
        }

        private void BtnStartStaticEventPublisher_Click(object sender, EventArgs e)
        {

            for (int i = 0; i < 100; i++)
            {
                StaticEventPublisher.OnSomething += new Subscriber().ShowMessage;

                var publisher = new StaticEventPublisher();
                publisher.TriggerSomething();
                StaticEventPublishers.Add(publisher);
            }
           
            MessageBox.Show(string.Format("There are {0} publisher in memory, {1} subscribers in memory", StaticEventPublisher.Count,Subscriber.Count));
        }

        private void btnGC_Click(object sender, EventArgs e)
        {
            GC.Collect();
        }
       
    }
}
