﻿using System;

namespace Event.MemoryLeak
{
    public class LongTimeService
    {
        public event EventHandler<EventArgs> SomeThingUpdated;

        protected virtual void OnDoSomeThing()
        {
            var handler = SomeThingUpdated;
            if (handler != null) handler(this, EventArgs.Empty);
        }
    }

    public class Runner
    {
        private LongTimeService _service;

        public Runner()
        {
             _service = new LongTimeService();
            
        }
        public void Run()
        {
            _service.SomeThingUpdated += (o, e) => { /*do some thing*/};
            _service.SomeThingUpdated += (o, e) => { /*do some thing*/};
            _service.SomeThingUpdated += (o, e) => { /*do some thing*/};
            _service.SomeThingUpdated += (o, e) => { /*do some thing*/};
        }
    }
}