﻿using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Event.MemoryLeak
{
    public class Subscriber
    {
        public string Text { get; set; }
        public List<StringBuilder> List = new List<StringBuilder>();
        public static int Count;
        public Subscriber()
        {
            Interlocked.Increment(ref Count);
            for (int i = 0; i < 1000; i++)
            {
                List.Add(new StringBuilder(1024));
            }
        }

        public void ShowMessage(object sender, PublisherEventArgs e)
        {
            Text = string.Format("There are {0} publisher in memory",e.PublisherReferenceCount);
        }

        ~Subscriber()
        {
            Interlocked.Decrement(ref Count);
        }
    }
}