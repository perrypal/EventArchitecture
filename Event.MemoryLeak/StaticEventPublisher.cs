﻿using System;
using System.Threading;

namespace Event.MemoryLeak
{
    public class StaticEventPublisher
    {
        public static int Count;

        public static event EventHandler<PublisherEventArgs> OnSomething;

        public StaticEventPublisher()
        {
            Interlocked.Increment(ref Count);
        }

        public void TriggerSomething()
        {
            RaiseOnSomething(new PublisherEventArgs(Count));
        }

        protected void RaiseOnSomething(PublisherEventArgs e)
        {
            EventHandler<PublisherEventArgs> handler = OnSomething;
            if (handler != null) handler(this, e);
        }

        ~StaticEventPublisher()
        {
            Interlocked.Decrement(ref Count);
        }
    }

  
}