﻿namespace Event.MemoryLeak
{
    partial class MemoryLeakForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnStartShortTimePublisherTest = new System.Windows.Forms.Button();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.BtnStartLongTimePublisher = new System.Windows.Forms.Button();
            this.BtnStartStaticEventPublisher = new System.Windows.Forms.Button();
            this.btnGC = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnStartShortTimePublisherTest
            // 
            this.btnStartShortTimePublisherTest.Location = new System.Drawing.Point(213, 26);
            this.btnStartShortTimePublisherTest.Name = "btnStartShortTimePublisherTest";
            this.btnStartShortTimePublisherTest.Size = new System.Drawing.Size(203, 23);
            this.btnStartShortTimePublisherTest.TabIndex = 0;
            this.btnStartShortTimePublisherTest.Text = "StartShortTimePublisher";
            this.btnStartShortTimePublisherTest.UseVisualStyleBackColor = true;
            this.btnStartShortTimePublisherTest.Click += new System.EventHandler(this.btnStartShortTimePublisherTest_Click);
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(140, 191);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(338, 22);
            this.txtDescription.TabIndex = 2;
            // 
            // BtnStartLongTimePublisher
            // 
            this.BtnStartLongTimePublisher.Location = new System.Drawing.Point(213, 73);
            this.BtnStartLongTimePublisher.Name = "BtnStartLongTimePublisher";
            this.BtnStartLongTimePublisher.Size = new System.Drawing.Size(203, 23);
            this.BtnStartLongTimePublisher.TabIndex = 3;
            this.BtnStartLongTimePublisher.Text = "StartLongTimePublisher";
            this.BtnStartLongTimePublisher.UseVisualStyleBackColor = true;
            this.BtnStartLongTimePublisher.Click += new System.EventHandler(this.BtnStartLongTimePublisher_Click);
            // 
            // BtnStartStaticEventPublisher
            // 
            this.BtnStartStaticEventPublisher.Location = new System.Drawing.Point(213, 122);
            this.BtnStartStaticEventPublisher.Name = "BtnStartStaticEventPublisher";
            this.BtnStartStaticEventPublisher.Size = new System.Drawing.Size(203, 23);
            this.BtnStartStaticEventPublisher.TabIndex = 4;
            this.BtnStartStaticEventPublisher.Text = "StartStaticEventPublisher";
            this.BtnStartStaticEventPublisher.UseVisualStyleBackColor = true;
            this.BtnStartStaticEventPublisher.Click += new System.EventHandler(this.BtnStartStaticEventPublisher_Click);
            // 
            // btnGC
            // 
            this.btnGC.Location = new System.Drawing.Point(469, 72);
            this.btnGC.Name = "btnGC";
            this.btnGC.Size = new System.Drawing.Size(75, 23);
            this.btnGC.TabIndex = 5;
            this.btnGC.Text = "GC";
            this.btnGC.UseVisualStyleBackColor = true;
            this.btnGC.Click += new System.EventHandler(this.btnGC_Click);
            // 
            // MemoryLeakForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(627, 242);
            this.Controls.Add(this.btnGC);
            this.Controls.Add(this.BtnStartStaticEventPublisher);
            this.Controls.Add(this.BtnStartLongTimePublisher);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.btnStartShortTimePublisherTest);
            this.Name = "MemoryLeakForm";
            this.Text = "MemoryLeakForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnStartShortTimePublisherTest;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.Button BtnStartLongTimePublisher;
        private System.Windows.Forms.Button BtnStartStaticEventPublisher;
        private System.Windows.Forms.Button btnGC;
    }
}

