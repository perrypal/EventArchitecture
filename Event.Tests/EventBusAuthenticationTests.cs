﻿using System;
using Castle.MicroKernel.Lifestyle;
using Castle.Windsor;
using Castle.Windsor.Installer;
using Event.Authentication;
using Event.Authentication.ContainerInstallers;
using Event.Authentication.EventBusPattern;
using NUnit.Framework;

namespace Event.Tests
{
    [TestFixture]
    public class EventBusAuthenticationTests
    {
        private WindsorContainer _container;
        private IDisposable _scope;

        [SetUp]
         public void SetupContainer()
        {
            _container = new WindsorContainer();
            _container.Install(FromAssembly.Containing<ObservedServiceInstaller>());
            _scope=_container.BeginScope();
        }

        [TearDown]
        public void DisposeScope()
        {
            _scope.Dispose();
        }

        [Test]
        public void After_register_user_other_observed_service_should_execute()
        {
            var registerService = _container.Resolve<UserService>();

            registerService.RegisterObservers();
            registerService.Register(new UserDomainModel());
        }
    }
}