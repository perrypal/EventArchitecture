﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Event.EventBus;
using FluentAssertions;
using NUnit.Framework;

namespace Event.Tests
{
    public class SimpleEventBusTests
    {
        [Test]
        public void Should_handle_registered_action()
        {
            var eventBus = new SimpleEventBus();

            var number = 0;
            eventBus.Register<MessageA>(m=>number=m.Number);
            eventBus.Publish(new MessageA(2));

            number.Should().Be(2);
        }

        internal class MessageA
        {
            public MessageA(int number)
            {
                Number = number;
            }
            public int Number { get; private set; }
        }
    }
}
