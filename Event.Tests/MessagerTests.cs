﻿using Event.EventBus;
using FluentAssertions;
using NUnit.Framework;

namespace Event.Tests
{
    public class MessagerTests
    {
        [Test]
        public void Should_handle_registered_actions()
        {
            int number = 0;
            Messenger.Default.Register<MessageA>(this,m=>number=m.Number);
            Messenger.Default.Send(new MessageA(2));

            number.Should().Be(2);
        }

        internal class MessageA
        {
            public MessageA(int number)
            {
                Number = number;
            }
            public int Number { get; private set; }
        }

        [Test]
        public void Should_handle_registered_action_in_subscriber()
        {
            var subscriber=new Subscriber();
            Messenger.Default.Register<MessageA>(this, subscriber.SetNumber);

            Messenger.Default.Send(new MessageA(2));

            subscriber.Number.Should().Be(2);
        }

       

        internal class Subscriber
        {
            public void SetNumber(MessageA messageA)
            {
                Number = messageA.Number;
            }

            public int Number { get; private set; }
        }
    }
}