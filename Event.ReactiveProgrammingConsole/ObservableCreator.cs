﻿using System.Reactive.Linq;
using Console = System.Console;
using System;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using System.Threading;

namespace Event.ReactiveProgrammingConsole
{
    public class ObservableCreator
    {
        public static void UsingReturn()
        {
            var greeting = Observable.Return("Hello world");
            greeting.Subscribe(Console.WriteLine);
        }

        public static void UsingCreate()
        {
            var greeting = Observable.Create<string>(observer =>
            {
                observer.OnNext("Hello world");
                return Disposable.Create(() => Console.WriteLine("Observer has unsubscribed"));
            });

            greeting.Subscribe(Console.WriteLine);
        }

        public static void UsingRange()
        {
            var range = Observable.Range(10, 15);
            range.Subscribe(Console.WriteLine, () => Console.WriteLine("Completed"));
        }

        public static void UsingGenerate()
        {
            var range = Observable.Generate(0, x => x < 10, x => x + 1, x => x);
            range.Subscribe(Console.WriteLine);
        }

        public static void UsingInterval()
        {
            var interval = Observable.Interval(TimeSpan.FromSeconds(1));
            interval.Subscribe(Console.WriteLine,() => Console.WriteLine("completed"));
        }

        public static void UsingTimer()
        {
            var timer = Observable.Timer(DateTimeOffset.Now,TimeSpan.FromSeconds(1));
            timer.Subscribe(Console.WriteLine,() => Console.WriteLine("completed"));
        }

        public static void UsingStart()
        {
            var start = Observable.Start(() => "Hello world");
            start.Subscribe(Console.WriteLine,() => Console.WriteLine("Action completed"));

        }

        public void RegisterOnErrorAndOnCompleted()
        {
            Observable.Range(1, 10)
           .Subscribe(x => Console.WriteLine(x.ToString()), e => Console.WriteLine("Error" + e.Message), () => Console.WriteLine("Completed"));
        }

        public void ChangeEnumerableToObservable()
        {
            Enumerable.Range(1, 10).ToObservable()
                .Subscribe(x => Console.WriteLine(x.ToString()));
        }

        public void ChangeObservableToEnumerable()
        {
            var list = Observable.Range(1, 10).ToEnumerable();
        }
    }
}