﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using Event.ReactiveProgrammingConsole.PingPang;

namespace Event.ReactiveProgrammingConsole
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            //ObservableCreator.UsingReturn();
            //ObservableCreator.UsingCreate();
            //ObservableCreator.UsingRange();
            //ObservableCreator.UsingGenerate();
            //ObservableCreator.UsingInterval();
            //ObservableCreator.UsingTimer();
            //ObservableCreator.UsingStart();

            //SchedulerSamples.UsingScheduler();
            SchedulerSamples.DifferenceBetweenSubscribeOnAndObserveOn();


            Console.ReadLine();
        }

       


    }
}
