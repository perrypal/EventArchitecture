﻿using System;
using System.Reactive.Linq;
using System.Reactive.Subjects;

namespace Event.ReactiveProgrammingConsole.PingPang
{
     class Ping:ISubject<Pong, Ping>
    {
        public void OnNext(Pong value)
        {
            Console.WriteLine("Ping received Pong.");
        }

        public void OnError(Exception exception)
        {
            Console.WriteLine("Ping experienced an exception and had to quit playing.");
        }

        public void OnCompleted()
        {
            Console.WriteLine("Ping finished.");
        }

        public IDisposable Subscribe(IObserver<Ping> observer)
        {
            return Observable.Interval(TimeSpan.FromSeconds(2))
                .Where(n => n < 10)
                .Select(n => this)
                .Subscribe(observer);
        }

        public void Dispose()
        {
            OnCompleted();
        }
    }
}