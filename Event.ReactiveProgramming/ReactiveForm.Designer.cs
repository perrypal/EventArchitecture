﻿namespace Event.ReactiveProgramming
{
    partial class ReactiveForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.btnFirstReactiveMode = new System.Windows.Forms.Button();
            this.btnFirstEventMode = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblMessage = new System.Windows.Forms.Label();
            this.btnSecondAsyncAwaitMode = new System.Windows.Forms.Button();
            this.BtnSecondEventAsyncModel = new System.Windows.Forms.Button();
            this.btnSecondReactiveMode = new System.Windows.Forms.Button();
            this.btnSecondEventMode = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnDecrement = new System.Windows.Forms.Button();
            this.BtnIncreasement = new System.Windows.Forms.Button();
            this.lblResult = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.GroupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // GroupBox1
            // 
            this.GroupBox1.Controls.Add(this.btnFirstReactiveMode);
            this.GroupBox1.Controls.Add(this.btnFirstEventMode);
            this.GroupBox1.Location = new System.Drawing.Point(51, 34);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(319, 131);
            this.GroupBox1.TabIndex = 0;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = "1.事件模型和反应式模型";
            // 
            // btnFirstReactiveMode
            // 
            this.btnFirstReactiveMode.Location = new System.Drawing.Point(182, 45);
            this.btnFirstReactiveMode.Name = "btnFirstReactiveMode";
            this.btnFirstReactiveMode.Size = new System.Drawing.Size(109, 23);
            this.btnFirstReactiveMode.TabIndex = 1;
            this.btnFirstReactiveMode.Text = "反应式模型";
            this.btnFirstReactiveMode.UseVisualStyleBackColor = true;
            // 
            // btnFirstEventMode
            // 
            this.btnFirstEventMode.Location = new System.Drawing.Point(53, 46);
            this.btnFirstEventMode.Name = "btnFirstEventMode";
            this.btnFirstEventMode.Size = new System.Drawing.Size(75, 23);
            this.btnFirstEventMode.TabIndex = 0;
            this.btnFirstEventMode.Text = "事件模型";
            this.btnFirstEventMode.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblMessage);
            this.groupBox2.Controls.Add(this.btnSecondAsyncAwaitMode);
            this.groupBox2.Controls.Add(this.BtnSecondEventAsyncModel);
            this.groupBox2.Controls.Add(this.btnSecondReactiveMode);
            this.groupBox2.Controls.Add(this.btnSecondEventMode);
            this.groupBox2.Location = new System.Drawing.Point(458, 34);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(343, 170);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "2.异步事件模型和反应式模型";
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Location = new System.Drawing.Point(148, 140);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(63, 17);
            this.lblMessage.TabIndex = 4;
            this.lblMessage.Text = "waiting...";
            // 
            // btnSecondAsyncAwaitMode
            // 
            this.btnSecondAsyncAwaitMode.Location = new System.Drawing.Point(18, 107);
            this.btnSecondAsyncAwaitMode.Name = "btnSecondAsyncAwaitMode";
            this.btnSecondAsyncAwaitMode.Size = new System.Drawing.Size(155, 23);
            this.btnSecondAsyncAwaitMode.TabIndex = 3;
            this.btnSecondAsyncAwaitMode.Text = "Async/Wait模型";
            this.btnSecondAsyncAwaitMode.UseVisualStyleBackColor = true;
            // 
            // BtnSecondEventAsyncModel
            // 
            this.BtnSecondEventAsyncModel.Location = new System.Drawing.Point(18, 75);
            this.BtnSecondEventAsyncModel.Name = "BtnSecondEventAsyncModel";
            this.BtnSecondEventAsyncModel.Size = new System.Drawing.Size(155, 23);
            this.BtnSecondEventAsyncModel.TabIndex = 2;
            this.BtnSecondEventAsyncModel.Text = "事件模型异步";
            this.BtnSecondEventAsyncModel.UseVisualStyleBackColor = true;
            // 
            // btnSecondReactiveMode
            // 
            this.btnSecondReactiveMode.Location = new System.Drawing.Point(201, 45);
            this.btnSecondReactiveMode.Name = "btnSecondReactiveMode";
            this.btnSecondReactiveMode.Size = new System.Drawing.Size(123, 23);
            this.btnSecondReactiveMode.TabIndex = 1;
            this.btnSecondReactiveMode.Text = "反应式模型";
            this.btnSecondReactiveMode.UseVisualStyleBackColor = true;
            // 
            // btnSecondEventMode
            // 
            this.btnSecondEventMode.Location = new System.Drawing.Point(18, 45);
            this.btnSecondEventMode.Name = "btnSecondEventMode";
            this.btnSecondEventMode.Size = new System.Drawing.Size(155, 23);
            this.btnSecondEventMode.TabIndex = 0;
            this.btnSecondEventMode.Text = "事件模型";
            this.btnSecondEventMode.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnDecrement);
            this.groupBox3.Controls.Add(this.BtnIncreasement);
            this.groupBox3.Controls.Add(this.lblResult);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Location = new System.Drawing.Point(39, 185);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(331, 100);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "3.事件流";
            // 
            // btnDecrement
            // 
            this.btnDecrement.Location = new System.Drawing.Point(146, 57);
            this.btnDecrement.Name = "btnDecrement";
            this.btnDecrement.Size = new System.Drawing.Size(75, 23);
            this.btnDecrement.TabIndex = 3;
            this.btnDecrement.Text = "-";
            this.btnDecrement.UseVisualStyleBackColor = true;
            // 
            // BtnIncreasement
            // 
            this.BtnIncreasement.Location = new System.Drawing.Point(53, 57);
            this.BtnIncreasement.Name = "BtnIncreasement";
            this.BtnIncreasement.Size = new System.Drawing.Size(75, 23);
            this.BtnIncreasement.TabIndex = 2;
            this.BtnIncreasement.Text = "+";
            this.BtnIncreasement.UseVisualStyleBackColor = true;
            // 
            // lblResult
            // 
            this.lblResult.AutoSize = true;
            this.lblResult.Location = new System.Drawing.Point(125, 18);
            this.lblResult.Name = "lblResult";
            this.lblResult.Size = new System.Drawing.Size(16, 17);
            this.lblResult.TabIndex = 1;
            this.lblResult.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(82, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "计数：";
            // 
            // ReactiveForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(827, 320);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.GroupBox1);
            this.Name = "ReactiveForm";
            this.Text = "ReactiveForm";
            this.GroupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GroupBox1;
        private System.Windows.Forms.Button btnFirstReactiveMode;
        private System.Windows.Forms.Button btnFirstEventMode;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnSecondEventMode;
        private System.Windows.Forms.Button btnSecondReactiveMode;
        private System.Windows.Forms.Button BtnSecondEventAsyncModel;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnDecrement;
        private System.Windows.Forms.Button BtnIncreasement;
        private System.Windows.Forms.Label lblResult;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSecondAsyncAwaitMode;
        private System.Windows.Forms.Label lblMessage;
    }
}