﻿using System;
using System.IO;
using System.Linq;
using System.Reactive.Linq;

namespace Event.ReactiveProgramming
{
    public class StartToUsingReactive
    {
        private void UsingFileWatcher()
        {
            //using event
            var watch = new FileSystemWatcher();
            watch.Created += (s, e) =>
            {
                var fileType = Path.GetExtension(e.FullPath);
                if (fileType.ToLower() == "jpg")
                {
                    //do some thing
                }
            };


            //using rx
            Observable.FromEventPattern<FileSystemEventArgs>(watch, "Created")
                .Where(e => Path.GetExtension(e.EventArgs.FullPath).ToLower() == "jpg")
                .Subscribe(e =>
                {
                    //do some thing
                });
        }

        private void UsingLinq()
        {
            var list = Enumerable.Range(1, 10)
                .Where(x => x > 8)
                .Select(x => x.ToString())
                .First();
        }
    }
}