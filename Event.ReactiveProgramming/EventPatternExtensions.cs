﻿using System;
using System.Reactive;
using System.Reactive.Linq;
using System.Windows.Forms;

namespace Event.ReactiveProgramming
{
    public static class EventPatternExtensions
    {
         public static IObservable<EventPattern<EventArgs>> FromClickEventPattern(this Button button)
         {
             return Observable.FromEventPattern<EventArgs>(button, "Click");
         }

         public static IObservable<EventPattern<EventArgs>> FromDoubleClickEventPattern(this Button button)
         {
             return Observable.FromEventPattern<EventArgs>(button, "DoubleClick");
         }
    }
}