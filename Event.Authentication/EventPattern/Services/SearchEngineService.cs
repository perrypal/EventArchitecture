﻿using System;

namespace Event.Authentication.EventPattern.Services
{
    public class SearchEngineService
    {
        private readonly UserService _userService;

        public SearchEngineService(UserService userService)
        {
            _userService = userService;
            _userService.UserRegistered += Insert;
        }

        public void Insert(UserRegisteredEventArgs user)
        {
            Console.WriteLine("Insert into search engine");
        } 
    }
}