﻿using System;

namespace Event.Authentication.EventPattern.Services
{
    public class RewardService
    {
        public RewardService(UserService userService)
        {
            userService.UserRegistered += Reward;
        }

        public void Reward(UserRegisteredEventArgs user)
        {
            if (ShouldReward(user))
            {
                Console.WriteLine("reward successfully");
            }
        } 

        private bool ShouldReward(UserRegisteredEventArgs user)
        {
            return true;
        }
    }
}