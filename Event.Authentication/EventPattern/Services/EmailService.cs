﻿using System;

namespace Event.Authentication.EventPattern.Services
{
    public class EmailService
    {
        public EmailService(UserService userService)
        {
            userService.UserRegistered += SendEmail;
        }

        public void SendEmail(UserRegisteredEventArgs user)
        {
            Console.WriteLine("send email successfully");
        } 
    }
}