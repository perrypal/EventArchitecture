﻿using System;
using Event.Authentication.NormalPattern;

namespace Event.Authentication.EventPattern
{
    public class UserService
    {
        private readonly UserRepository _userRepository;
        public event Action<UserRegisteredEventArgs> UserRegistered;

        public UserService(UserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public void Register(UserDomainModel user)
        {
            var userDomain= User.Register(user);
            _userRepository.Save(userDomain);

            OnUserRegistered(new UserRegisteredEventArgs());
        }

        protected virtual void OnUserRegistered(UserRegisteredEventArgs e)
        {
            var handler = UserRegistered;
            if (handler != null) handler(e);
        }
    }
}