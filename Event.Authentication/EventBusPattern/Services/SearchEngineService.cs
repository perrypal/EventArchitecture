﻿using System;
using Event.EventBus;

namespace Event.Authentication.EventBusPattern.Services
{
    public class SearchEngineService
    {
        public SearchEngineService(UserService userService, IMessenger messenger)
        {
            messenger.Register<UserRegisteredMessage>(userService, Insert);
        }

        public void Insert(UserRegisteredMessage user)
        {
            Console.WriteLine("Insert into search engine");
        }
    }

}