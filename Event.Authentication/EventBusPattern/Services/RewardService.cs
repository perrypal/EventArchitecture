﻿using System;
using Event.EventBus;

namespace Event.Authentication.EventBusPattern.Services
{
    public class RewardService 
    {
        public RewardService(UserService userService,IMessenger messenger)
        {
            messenger.Register<UserRegisteredMessage>(userService, Reward);
        }

        public void Reward(UserRegisteredMessage user)
        {
            if (ShouldReward(user))
            {
                Console.WriteLine("reward successfully");
            }
        }

        private bool ShouldReward(UserRegisteredMessage user)
        {
            return true;
        }

       
    }
}