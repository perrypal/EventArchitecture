﻿using System;
using Event.EventBus;

namespace Event.Authentication.EventBusPattern.Services
{
    public class EmailService
    {
        public EmailService(UserService userService,IMessenger messenger)
        {
            messenger.Register<UserRegisteredMessage>(userService, SendEmail);
        }

        public void SendEmail(UserRegisteredMessage user)
        {
            Console.WriteLine("send email successfully");
        }

     
    }
}