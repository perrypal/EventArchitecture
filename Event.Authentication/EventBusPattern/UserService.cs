﻿using System;
using Castle.Core.Internal;
using Castle.Windsor;
using Event.EventBus;

namespace Event.Authentication.EventBusPattern
{
    public class UserService
    {
        private readonly UserRepository _userRepository;
        private readonly IMessenger _messenger;

        public UserService(UserRepository userRepository,IMessenger messenger)
        {
            _userRepository = userRepository;
            _messenger = messenger;
        }

        public void Register(UserDomainModel user)
        {
            var userDomain= User.Register(user);
            _userRepository.Save(userDomain);

            _messenger.Send(new UserRegisteredMessage());
        }
      
    }
}