﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Event.EventBus;

namespace Event.Authentication.ContainerInstallers
{
    public class EventBusInstaller:IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<IMessenger>().Instance(Messenger.Default).LifestyleScoped());
        }
    }
}