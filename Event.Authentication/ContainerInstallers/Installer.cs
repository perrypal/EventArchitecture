﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Event.Authentication.EventBusPattern;

namespace Event.Authentication.ContainerInstallers
{
    public class Installer:IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<UserService>().LifestyleScoped());
            container.Register(Component.For<UserRepository>().LifestyleScoped());
            container.Register(Component.For<IWindsorContainer>().Instance(container).LifestyleScoped());
        }
    }
}