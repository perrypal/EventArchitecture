﻿using Event.Authentication.EventPattern;
using Event.Authentication.NormalPattern.Services;

namespace Event.Authentication.NormalPattern
{
    public class RegisterService
    {
        private readonly UserRepository _userRepository;
        private readonly EmailService _emailService;
        private readonly SearchEngineService _searchEngineService;
        private readonly RewardService _rewardService;

        public RegisterService(UserRepository userRepository,EmailService emailService,SearchEngineService searchEngineService,RewardService rewardService)
        {
            _userRepository = userRepository;
            _emailService = emailService;
            _searchEngineService = searchEngineService;
            _rewardService = rewardService;
        }

        public void Register(UserDomainModel user)
        {
            var userDomain= User.Register(user);
            _userRepository.Save(userDomain);

            _emailService.SendEmail(user);
            _searchEngineService.Insert(user);
            _rewardService.Reward(user);
        }
    }
}