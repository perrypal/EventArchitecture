﻿using System;

namespace Event.Authentication.NormalPattern.Services
{
    public class SearchEngineService
    {
        public void Insert(UserDomainModel user)
        {
            Console.WriteLine("Insert into search engine");
        } 
    }
}