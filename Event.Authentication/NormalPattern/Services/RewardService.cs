﻿using System;

namespace Event.Authentication.NormalPattern.Services
{
    public class RewardService
    {
        public void Reward(UserDomainModel user)
        {
            if (ShouldReward(user))
            {
                Console.WriteLine("reward successfully");
            }
        }

        private bool ShouldReward(UserDomainModel user)
        {
            return true;
        }
    }
}