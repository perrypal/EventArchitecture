﻿using Event.Authentication.NormalPattern.Services;
using Event.ServiceBus;

namespace Event.Authentication.ServiceBusPattern
{
    public class RegisterService
    {
        private readonly UserRepository _userRepository;
        private readonly IServiceBus _serviceBus;

        public RegisterService(UserRepository userRepository,IServiceBus serviceBus)
        {
            _userRepository = userRepository;
            _serviceBus = serviceBus;
        }

        public void Register(UserDomainModel user)
        {
            var userDomain= User.Register(user);
            _userRepository.Save(userDomain);

           _serviceBus.Publish(new UserRegisteredMessage());
        }
    }
}