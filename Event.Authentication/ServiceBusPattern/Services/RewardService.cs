﻿using System;
using System.Runtime.Remoting.Messaging;
using Event.ServiceBus;

namespace Event.Authentication.ServiceBusPattern.Services
{
    public class RewardService : IMessageHandler<UserRegisteredMessage>
    {
        private bool ShouldReward(UserRegisteredMessage user)
        {
            return true;
        }

        public void Handle(UserRegisteredMessage user)
        {
            if (ShouldReward(user))
            {
                Console.WriteLine("reward successfully");
            }
        }
    }
}