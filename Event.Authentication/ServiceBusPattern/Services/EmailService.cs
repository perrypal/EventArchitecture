﻿using System;
using Event.ServiceBus;

namespace Event.Authentication.ServiceBusPattern.Services
{
    public class EmailService : IMessageHandler<UserRegisteredMessage>
    {
        public void Handle(UserRegisteredMessage user)
        {
            Console.WriteLine("send email successfully");
        } 
    }
}