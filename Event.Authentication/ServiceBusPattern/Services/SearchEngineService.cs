﻿using System;
using System.Runtime.Remoting.Messaging;
using Event.ServiceBus;

namespace Event.Authentication.ServiceBusPattern.Services
{
    public class SearchEngineService : IMessageHandler<UserRegisteredMessage>
    {
        public void Handle(UserRegisteredMessage user)
        {
            Console.WriteLine("Insert into search engine");
        }
    }
}