﻿using System;

namespace Event.EventBus
{
    public interface IMessenger
    {
        void Register<TMessage>(object recipient, Action<TMessage> action);
        
        void Send<TMessage>(TMessage message);
    }
}