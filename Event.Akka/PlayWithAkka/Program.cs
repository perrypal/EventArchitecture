﻿using System;
using Akka.Actor;
using Event.PlayWithAkka.SayHelloWorld;

// ReSharper disable once IdentifierTypo
namespace Event.PlayWithAkka
{
    class Program
    {
        static void Main(string[] args)
        {
            new Bootstrap().Start();

            Console.ReadLine();
        }
    }
}
