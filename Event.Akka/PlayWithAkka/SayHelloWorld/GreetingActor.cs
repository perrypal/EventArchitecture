﻿using System;
using Akka.Actor;

namespace Event.PlayWithAkka.SayHelloWorld
{
    public class GreetingActor : ReceiveActor
    {
        public GreetingActor()
        {
            Receive<GreetingMessage>(greet =>Console.WriteLine("Hello World"));
        }
    }
}