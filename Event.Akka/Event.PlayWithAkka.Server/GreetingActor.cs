﻿using System;
using Akka.Actor;
using Event.PlayWithAkka.Common;

namespace Event.PlayWithAkka.Server
{
    public class GreetingActor:TypedActor,IHandle<GreetingMessage>
    {
        public void Handle(GreetingMessage message)
        {
            Console.WriteLine("Hello world!");
        }
    }
}